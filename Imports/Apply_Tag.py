import Wake_Up

list_tag = ['Tag_1', 'Tag_2', 'Tag_3']


def print_tag(list_tag):
    print "\nTags :\n"
    i = 0
    while i < len(list_tag):
        print " [" + str(i) + "] " + str(list_tag[i])
        i += 1

def apply_tag(mc, system):

    print_tag(list_tag)

    id_tag = raw_input("\nTag to apply : ")

    print "\nTry to apply tag " + list_tag[int(id_tag)] + " on " + system + "...\n"

    try:
        apply = mc.system.applyTag(system, list_tag[int(id_tag)])
        print apply
        if apply == 0:
            print "Tag " + str(list_tag[int(id_tag)]) + " applied on " + system
    except Exception, e:
        print 'Error ' + str(e)

    Wake_Up.wake_up(mc, system)
    raw_input()

def remove_tag(mc, system):

    print_tag(list_tag)

    id_tag = raw_input("\nTag to remove : ")

    print "\nTry to remove tag " + list_tag[int(id_tag)] + " on " + system + "...\n"

    try:
        remove = mc.system.clearTag(system, list_tag[int(id_tag)])
        print remove
        if remove == 0:
            print "Tag : " + str(list_tag[int(id_tag)]) + " removed on " + system
    except Exception, e:
        print 'Error ' + str(e)

    Wake_Up.wake_up(mc, system)
    raw_input()
