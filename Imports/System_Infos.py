
def system_infos(mc, system):
    print "\nSystem infos on " + system + " :\n"
    try:
        commande = mc.system.find(system)
        # print commande
        for info in commande:
            print "Computer Name : " + info['EPOComputerProperties.ComputerName']
            print "Last Update : " + info['EPOLeafNode.LastUpdate']
            print "IP Address : " + info['EPOComputerProperties.IPAddress']
            print "Username : " + info['EPOComputerProperties.UserName']
            print "Tags : " + info['EPOLeafNode.Tags']
            print "AgentVersion : " + info['EPOLeafNode.AgentVersion']
            print "Domain : " + info['EPOComputerProperties.DomainName']
            print "OS : " + info['EPOComputerProperties.OSType']
            print "Branch Node ID : " + str(info['EPOBranchNode.AutoID'])
    except Exception, e:
        print 'Error ' + str(e)
        # continue

    raw_input()