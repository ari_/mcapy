import Wake_Up

List = "systems.txt"

def task_on_list(mc):
    print "\n [0] Apply Tag"
    print " [1] Wake Up Agent"
    print " [2] Wake Up Agent by Tag"
    print " [3] Last connexion"
    print " [4] Apply Tag & move systems"

    choice = raw_input("\nChoice : ")
    if choice == "1" or choice == "3" or choice == "4":
        raw_input("\nMake sure that you have edited correctly the 'system.txt' file and press ENTER !")

    if choice == "0":
        tag = raw_input("Tag to apply : ")
        systems = open(List, 'r')
        for system in systems:
            print "\nTry to apply tag on " + system + "...\n"
            try:
                print mc.system.applyTag(system.rstrip('\n'), tag)
            except Exception, e:
                print 'Error ' + str(e)
        raw_input()

    elif choice == "1":
        systems = open(List, 'r')
        for system in systems:
            Wake_Up.wake_up(mc, system.rstrip('\n'))
        raw_input()


    elif choice == "2":
        tag = raw_input("\nTag of systems to wake up : ")
        try:
            systems = mc.system.find(tag)
            print "\n List of systems with your tag " + tag + " : \n"
            for info in systems:
                print info['EPOComputerProperties.ComputerName']

            confirmation = raw_input("Are you sure to wake up systems : [y/n] ")
            if confirmation == "y":
                for system in systems:
                    id = system['EPOComputerProperties.ParentID']
                    print Wake_Up.wake_up(mc, id)
                raw_input()
        except Exception, e:
                print 'Error ' + str(e)

