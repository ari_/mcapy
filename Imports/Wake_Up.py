def wake_up(mc, system):
    print "\nTry to wake up " + system + "...\n"
    try:
        print mc.system.wakeupAgent(system, abortAfterMinutes="1")
    except Exception, e:
        print 'Error ' + str(e)
    # raw_input()
