def epo_management(mc):
    print "\n [0] List of Users"
    print " [1] User's information"
    print " [2] Disable User"
    print " [3] Remove User"
    print " [4] Add User"

    choice = raw_input("\nChoice : ")

    if choice == "0":
        print "\nUsers : "
        try:
            users = mc.core.listUsers()
            # print "Users : " + str(users)
            for user in users:
                print "User : " + user['name']
        except Exception, e:
            print 'Error ' + str(e)
        raw_input()

    elif choice == "1":
        username = raw_input("\nUsername : ")
        try:
            users = mc.core.listUsers()
            for user in users:
                if username == str(user['name']):
                    print user
                    print "\n"
                    print "Username : " + user['name']
                    print "Auth Type : " + user['authType']
                    print "Auth Details : " + user['authDetails']
                    print "Is admin : " + str(user['admin'])
                    print "Notes : " + user['notes']
                    print "ID : " + str(user['id'])
                    print "Disabled : " + str(user['disabled'])
                    print "Phone number : " + user['phoneNumber']
                    print "Fullname : " + user['fullName']
                    print "Email : " + user['email']

        except Exception, e:
            print 'Error ' + str(e)
        raw_input()

    elif choice == "2":
        user = raw_input("\nUsername : ")
        try:
            print mc.core.updateUser(user, disabled="True")
        except Exception, e:
            print 'Error ' + str(e)
        raw_input()

    elif choice == "3":
        user = raw_input("\nUsername : ")
        try:
            print mc.core.removeUser(user)
        except Exception, e:
            print 'Error ' + str(e)
        raw_input()

    elif choice == "4":
        username = raw_input("\nUsername : ")
        password = raw_input("Password : ")
        fullname = raw_input("Fullname : ")
        mail = raw_input("Email :")
        phonenumber = raw_input("Phone Number : ")
        note = raw_input("Notes : ")
        disable = raw_input("Disabled : [True/False] ")
        adm = raw_input("Admin : [True/False] ")
        retrytolerant = raw_input("Retrytolerant : [True/False] ")
        try:
            print mc.core.addUser(username, password, fullName=fullname, email=mail, phoneNumber=phonenumber, notes=note, disabled=disable, admin=adm, retryTolerant=retrytolerant)
        except Exception, e:
                print 'Error ' + str(e)
        raw_input()
