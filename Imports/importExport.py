import mcafee

server_to_import = ""
port_to_import = ""
username = ""
password = ""

def import_export(mc):

    print "\n [0] Import Tasks & Policies"
    print " [1] Export Tasks & Policies\n"

    choice = raw_input("Choice : ")

    if choice == "0":
        confirmation = raw_input("Are you sure to import tasks & policies : [y/n] ")
        if confirmation == "y":
			server_to_import = raw_input("Server : ")
			port_to_import = raw_input("Port : ")
            username = raw_input("Username : ")
            password = raw_input("Password : ")
            try:
                mc2 = mcafee.client(server_to_import, port_to_import, username, password, 'https', 'json')
                mc2.clienttask.importClientTask(uploadFile='tasks.xml')
                mc2.policy.importPolicy(file='policies.xml')
            except Exception, e:
                print 'Error ' + str(e)

    elif choice == "1":

        tasks = mc.clienttask.export()
        file = open('tasks.xml', 'w')
        print >>file, tasks
        #file.write(str(tasks))
        file.close()

        policies = mc.policy.export()
        file = open('policies.xml', 'w')
        print >>file, policies
        file.close()


