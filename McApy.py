# McAfee ePO python script
# Pour pouvoir copier dans la fenetre d'execution :
# propriete de la fenetre -> Option -> Option d'edition -> Mode d'edition rapide

import sys 
sys.path.append('Imports')
import mcafee
import getpass
import System_Infos
import Wake_Up
import Apply_Tag
import Task_On_List
import ePOManagement
import importExport

print " ___  ___      ___"
print " |  \/  |     / _ \ "
print " | .  . | ___/ /_\ \_ __  _   _ "
print " | |\/| |/ __|  _  | '_ \| | | |"
print " | |  | | (__| | | | |_) | |_| |"
print " \_|  |_/\___\_| |_/ .__/ \__, |"
print "                   | |     __/ |"
print "                   |_|    |___/"
print "                                by ari_\n"

print "#Login\n"
# Infos server connexion :
username = raw_input("Username : ")
password = getpass.getpass("Password : ")
server = "a-ibepoe03.srv-ib.ibp"
port = "8681"
try:
    mc = mcafee.client(server, port, username, password)
except Exception, e:
    print 'Error ' + str(e)
    wait = raw_input()
    exit()

print "\no/ " + username

run = True
while run is True:
    print "\nMcApy_Menu :\n"
    print " [0] System Infos"
    print " [1] Wake Up Agent"
    print " [2] Apply Tag " # & move system" TODO
    print " [3] Remove Tag " # & move system" TODO
    print " [4] Task on a list of systems"
    print " [5] ePO Management"
    print " [6] Import & Export Tasks & Policies"
    print " [h] Help"
    print " [q] Quit\n"

    choice = raw_input("Choice : ")

    if choice == "0":
        system = raw_input("\nSystem : ")
        System_Infos.system_infos(mc, system)

    elif choice == "1":
        system = raw_input("\nSystem or Tag : ")
        Wake_Up.wake_up(mc, system)

    elif choice == "2":
        system = raw_input("\nSystem : ")
        Apply_Tag.apply_tag(mc, system)

    elif choice == "3":
        system = raw_input("\nSystem : ")
        Apply_Tag.remove_tag(mc, system)

    elif choice == "4":
        Task_On_List.task_on_list(mc)

    elif choice == "5":
        ePOManagement.epo_management(mc)

    elif choice == "6":
        importExport.import_export(mc)

    elif choice == "h":
        print "\nHelp : read the code !"

    elif choice == "q":
        run = False

    else:
        print "\nBad choice."




